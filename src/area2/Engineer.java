package area2;

public class Engineer extends Person {

	private int salary;
	
	public Engineer(String name, int age) {
		super(name, age);
		salary = 0;
	}
	
	public Engineer(String name, int age, int salary) {
		this(name, age);
		this.salary = salary;
	}
	
	// getters and setters
}
