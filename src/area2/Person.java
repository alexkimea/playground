package area2;

public class Person {
	
	private int age;
	private String name;
	private boolean isAlive;
	
	public Person() {
		isAlive = true;
	}
	
	public Person(String name, int age) {
		this();
		this.name = name;
		this.age = age;
	}
	
	// getters and setters
}
